import React, {Component} from 'react';
import './Home.scss';
import Singleipad from '../Singleipad/Singleipad';
import Doubleipad from '../Doubleipad/Doubleipad';
import Doubleipadland from '../Doubleipadland/Doubleipadland';
import Textimage from '../Textimage/Textimage';
import IpadGroups from '../IpadGroups/IpadGroups';
// import picularLand from '../../images/picular-land.png';
import abstractLand from '../../images/abstract-land.png';
import landClothes from '../../images/land-clothes.png';
import vertClothes from '../../images/vert-clothes.png';
// import landRes from '../../images/land-res.png';
// import landscape1 from '../../images/landscape1.png';
import landscape2 from '../../images/landscape3.jpg';
import landSearch from '../../images/land-search.png';
// import landDark from '../../images/land-dark.png';
// import landDark2 from '../../images/land-dark2.png';
import landDark3 from '../../images/land-dark3.png';
// import landDark4 from '../../images/land-dark4.png';
import landDark5 from '../../images/land-dark5.png';
import landDetail from '../../images/land-detail.png';
import landMessenger from '../../images/land-messenger.png';
// import wonderwoman from '../../images/wonder-woman.png';
import landCalendar from '../../images/land-calendar.png';
import bytedanceLogo from '../../images/bytedance-logo.png';
import tiktokVideo from '../../images/tiktok-video.mp4';

// import styled from 'styled-components';
import { Controller, Scene } from 'react-scrollmagic';
import { Tween, Timeline } from 'react-gsap';
class Home extends Component {
    render() {
        return (
            <div>
                <nav className="navbar navbar-expand-lg pt-4 pl-5 pr-5">
                    <h1 className="karla-font">Portfolio</h1>
                </nav>
                <section className="d-flex align-items-center section-height">
                    <div className="container p-5">
                        <h1 className="gradient-text font-weight-bold display-3">Hi! I'm Stephen.</h1>
                        <h5 className="line-height-4 mt-3 col-md-8 m-0 p-0 subtext-color bold-3">I'm an incoming front end developer of ByteDance based in Mountain View. I love designing UI and creating websites for different needs.</h5>
                    </div>
                </section>
                <Singleipad isVideo='false' title="Minimalism" description="Going minimal is the ultimate sophistication in websites. I am devoted to convey most information by using least contents for the users. My design embraces simplicity on every level." imageUrl={landMessenger}></Singleipad>
                <Doubleipad title="Responsive" description="My work consists of a mix of flexible grids and layouts, images and an intelligent use of CSS media queries. As the user switches their devices, the website should automatically switch to accommodate for resolution, image size and scripting abilities." imageUrlLeft={landClothes} imageUrlRight={vertClothes}></Doubleipad>
                <Singleipad isVideo='false' smallTitle = "Designed by Irina Krotkova" title="Aesthetic" description="Designing a beautiful web page can be tricky, but I'm here to help. I focus on high-quality images, modern typography and contrasting colors to create aesthetic user interfaces." imageUrl={landCalendar}></Singleipad>
                <div className="container">
                    <hr style={{borderTop: '1px solid #989898'}}></hr>
                </div>
                <section className="container pt-5 skills-container pb-5">
                    <div className="row align-items-center m-0">
                        <div className="col-md-12 p-0">
                            <h5 className="subsubtext-color bold-5">Web Development</h5>
                        </div>
                        <div className="col-md-6 p-0 mt-2">
                            <h1 className="bold-5">Frameworks</h1>
                        </div>
                        <div className="col-md-6 p-0 mt-2">
                            <div className="d-flex">
                                <h3 className="subtext-color bold-7">React</h3>
                                <h3 className="ml-4 subtext-color bold-7">Vue</h3>
                                <h3 className="ml-4 subtext-color bold-7">Angular</h3>
                            </div>
                        </div>
                    </div>
                    <div className="row align-items-center m-0 mt-5">
                        <div className="col-md-12 p-0">
                            <h5 className="subsubtext-color bold-5 mt-2">Programming</h5>
                        </div>
                        <div className="col-md-6 p-0 mt-2">
                            <h1 className="bold-5">Tools</h1>
                        </div>
                        <div className="col-md-6 p-0 mt-2">
                            <div className="d-flex">
                                <h3 className="subtext-color bold-7">HTML</h3>
                                <h3 className="ml-4 subtext-color bold-7">CSS</h3>
                                <h3 className="ml-4 subtext-color bold-7">JavaScript</h3>
                            </div>
                        </div>
                    </div>
                    <div className="row align-items-center m-0 mt-5">
                        <div className="col-md-12 p-0">
                            <h5 className="subsubtext-color bold-5">Visual</h5>
                        </div>
                        <div className="col-md-6 p-0 mt-2">
                            <h1 className="bold-5">Design</h1>
                        </div>
                        <div className="col-md-6 p-0 mt-2">
                            <div className="d-flex">
                                <h3 className="subtext-color bold-7">Adobe XD</h3>
                            </div>
                        </div>
                    </div>
                    <div className="single-ipad-extra pb-5"></div>
                </section>

                <section className="desktop-view pb-5">
                <div className="container p-5 d-flex flex-column align-items-center mb-3">
                    <h1 className="display-3 text-left bold-7 text-center mt-2">Animation</h1>
                    <h5 className="col-md-8 subtext-color bold-3 p-0 line-height-3 mt-2 text-center mb-3">Animation is complicated. I'm constantly looking at ways to optimise user experiences with different animations on text and images. There are no best ways to use animations, but only beautiful ways.</h5>
                </div>
                <Controller>
                    <Scene
                        triggerHook="onLeave"
                        duration={3000}
                        pin
                    >
                        {(progress) => (
                        <div className="sticky d-flex justify-content-center align-items-center">
                            <Timeline totalProgress={progress} paused>
                            <Tween
                                from={{ transform: 'scale(2)'}}
                                to={{ transform: 'scale(1)' }}
                            >
                                <div className="col-md-8 c-single-ipad pb-5 pt-5">
                                    <div className="ipad">
                                        <div className="position-absolute d-flex align-items-center justify-content-center" style={{top: '0', left: '0', right: '0', bottom: '0', zIndex: '10'}}>
                                            <Timeline
                                            target={
                                                <div className="col-md-8">
                                                    <h1 className="font-weight-bold text-center sf-pro-family display-4">Everything you can imagine is real.</h1>
                                                    <h6 className="text-center font-weight-bold sf-pro-family">Pablo Picasso</h6>
                                                </div>
                                            }>
                                                <Tween
                                                    from={{ opacity: 0 }}
                                                    to={{ opacity: 1 }}
                                                />
                                                <Tween
                                                    to={{ opacity: 0 }}
                                                />
                                            </Timeline>
                                        </div>
                                        <div>
                                            <img src={landscape2} alt="abstractLand"></img>
                                                <Tween
                                                    from={{ opacity: 0}}
                                                    to={{ opacity: 1 }}
                                                    delay={1}
                                                >
                                                    <img src={landSearch} alt="abstractLand" className="position-absolute" style={{zIndex: '2', top: '0', left: '0', right: '0'}}></img>
                                                </Tween>
                                        </div>
                                    </div>
                                    <div className="pb-5"></div>
                                </div>
                            </Tween>
                            </Timeline>
                        </div>
                        )}
                    </Scene>
                </Controller>
                </section>
                <section className="phone-view">
                    <Singleipad isVideo='false' title="Animation" description="Animation is complicated. I'm constantly looking at ways to optimise user experiences with different animations on text and images. There are no best ways to use animations, but only beautiful ways." imageUrl={landSearch}></Singleipad>
                </section>
                <Doubleipadland title="Dark Mode" description="Dark theme is one of the most requested features over the past few years. Most companies made a dark theme an essential part of UI. Dark theme’s reduced luminance provides safety in dark environments and can minimize eye strain." imageUrlLeft={landDark3} imageUrlRight={landDark5}></Doubleipadland>
                <Singleipad isVideo='false' title="User Experiences" description="I focus on anticipating what users might need to do and ensuring that the interface has elements that are easy to access, understand, and use to facilitate those actions. I am also devoted to improve the quality of the user’s interaction with and perceptions of your product and any related services." imageUrl={landDetail}></Singleipad>
                <Textimage title="Color Theory" description="Color is not only an incredibly powerful part of one's website, it plays a crucial role in defining the user's experience. The art of color is supposed to create a digital experience the users will better engage with." imageUrl={abstractLand}></Textimage>
                <IpadGroups></IpadGroups>
                <Singleipad smallTitle="Designed by Tiktok" isVideo="true" title="Contact" imageUrl={tiktokVideo}></Singleipad>
                <footer className="container p-2">
                    <hr style={{borderTop: '1px solid #1a1a1a'}}></hr>
                    <div className="row">
                        <div className="col-md-12 text-center">
                            <small><p className="subsubtext-color font-small bold-1">Copyright 2020 &#169; Stephen Fan. All rights reserved. Designed and coded by Stephen Fan.</p></small>
                        </div>
                    </div>
                </footer>
            </div>
        )
    }
}

export default Home;