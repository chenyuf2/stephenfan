import React, {Component} from 'react';
import './Singleipad.scss';

class Singleipad extends Component {
    render() {
        return (
            <section className="d-flex justify-content-center flex-column align-items-center overflow-hidden">
                    <div className="container p-5 d-flex flex-column align-items-center">
                            <h1 className="display-3 text-left bold-7 text-center mt-2">{this.props.title}</h1>
                            {
                                this.props.isVideo === 'false' ?
                                <h5 className="col-md-8 subtext-color bold-3 p-0 line-height-3 mt-2 text-center mb-3">{this.props.description}</h5> :
                                <h5 className="col-md-8 subtext-color bold-3 p-0 line-height-3 mt-2 text-center mb-3">I am more than happy to know more people who have same interests as mine. Send me an email by <span className="text-white font-weight-bold" style={{textDecoration:'underline', textDecorationColor: '#FC466B'}}>fcy82539870@gmail.com</span> to chat.</h5>
                            }
                    </div>
                    <div className="mt-3 pb-5 mb-5 col-md-8 c-single-ipad d-flex flex-column justify-content-center align-items-center">
                        <div className="ipad">
                            {
                                this.props.isVideo === 'false' ? 
                                <img src={this.props.imageUrl} alt="abstractLand"></img> :
                                <video autoPlay="true" loop="true" src={this.props.imageUrl} alt="abstractLand" muted></video>
                            }
                        </div> 
                        {this.props.smallTitle ? 
                         <div style={{marginTop: '-25px'}} className="w-100 text-right subtext-color font-weight-bold small-title-font">{this.props.smallTitle}</div> :
                         ''
                        }
                    </div>
                    <div className="single-ipad-extra pb-5 mb-5 pt-5"></div>
            </section>
        )
    }
}

export default Singleipad;