import React, {Component} from 'react';
import "./Textimage.scss";
class Textimage extends Component {
    render() {
        return (
            <section className="mb-5 pb-5 overflow-hidden">
                    <div className="container p-5 d-flex flex-column align-items-center">
                            <h1 className="display-3 text-left bold-7 text-center mt-2 text-gradient-color">{this.props.title}</h1>
                            <h5 className="col-md-8 subtext-color bold-3 p-0 line-height-3 mt-2 text-center mb-3">{this.props.description}</h5>
                    </div>
                    <div className="mt-3 pb-5 mb-5">
                        <div className="text-image-layout container pb-5">
                            <div className="ipad-landscape mb-5 ml-0 mr-0">
                                <div className="ipad">
                                    <img src={this.props.imageUrl} alt="abstractLand"></img>
                                </div>
                            </div>
                            <div className="left-text-box pl-4 pr-4 d-flex flex-column justify-content-center">
                                <div>
                                    <h3 className="font-weight-bold mt-2">Consistent</h3>
                                    <h5 className="subtext-color bold-3 line-height-3 mt-2">
                                        For all my designs, I apply a specfic color pattern throughout a UI consistently. Colors should also be compatible with the product it represents.
                                    </h5>
                                </div>
                                <div className="mt-5">
                                    <h3 className="font-weight-bold">Distinct</h3>
                                    <h5 className="subtext-color bold-3 line-height-3 mt-2">
                                        I create appropriate distinctions between elements, with sufficient color contrast between them.
                                    </h5>
                                </div>
                                <div className="mt-5">
                                    <h3 className="font-weight-bold">Intentional</h3>
                                    <h5 className="subtext-color bold-3 line-height-3 mt-2">
                                        I apply colors purposefully as they can convey meanings in multiple ways, such as relationships between elements and degrees of hierarchy.
                                    </h5>
                                </div>
                            </div>
                        </div>
                    </div>
            </section>
        )
    }
}

export default Textimage;