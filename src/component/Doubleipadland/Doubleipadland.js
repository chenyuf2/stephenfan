import React, {Component} from 'react';

class Doubleipadland extends Component {
    render() {
        return (
            <section className="d-flex justify-content-center flex-column align-items-center mb-5 pb-5 overflow-hidden">
                    <div className="container p-5 d-flex flex-column align-items-center">
                            <h1 className="display-3 text-left bold-7 text-center mt-2">{this.props.title}</h1>
                            <h5 className="col-md-8 subtext-color bold-3 p-0 line-height-3 mt-2 text-center mb-3">{this.props.description}</h5>
                    </div>
                    <div className="mt-3 pb-5 mb-5 col-md-8 c-double-ipad d-flex justify-content-center landscape-left">
                        <div className="double-ipad-column pb-5 mb-5">
                            <div className="ipad">
                                <img src={this.props.imageUrlLeft} alt="abstractLand"></img>
                            </div>
                        </div>
                        <div className="double-ipad-column pb-5 mb-5">
                            <div>
                                <div className="ipad">
                                    <img src={this.props.imageUrlRight} alt="abstractLand"></img>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="single-ipad-extra"></div>
            </section>
        )
    }
}

export default Doubleipadland;