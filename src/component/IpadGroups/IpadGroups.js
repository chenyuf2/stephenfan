import React, {Component} from 'react';
import './IpadGroups.scss';
import wonderwoman from '../../images/wonder-woman.png'
// import vertCloth from '../../images/vert-clothes.png'
import landEarth from '../../images/land-earth.png';
// import vertEarth from '../../images/vert-earth.png';
// import picular from '../../images/picular.png';
import landRes from '../../images/land-res.png';
import landInterstellar from '../../images/land-interstellar.png';
// import landCalendar from '../../images/land-calendar.png';
import landTiktok from '../../images/land-tiktok.png'
import landClothes from '../../images/land-clothes.png';
import landXiaoyi from '../../images/land-xiaoyi.png';
import dataAnalysis from '../../images/data-analysis.png';
import lotr3 from '../../images/lotr3.png';
// import tiktokVideo from '../../images/tiktok-video.mp4';
export default class IpadGroups extends Component {
    render() {
        return (
            <div className="mb-5 pb-5">
                <div className="container p-5 d-flex flex-column align-items-center">
                    <h1 className="display-3 text-left bold-7 text-center mt-2">Satisfaction</h1>
                    <h5 className="col-md-8 subtext-color bold-3 p-0 line-height-3 mt-2 text-center mb-3">Enjoy the beauty of web design and every corner of my projects. I am dedicated to bringing uniqueness to all of my works.</h5>
                </div>
                <div className="d-flex align-items-center justify-content-center mt-3">
                    <div className="col-md-4 ipad-groups-column">
                        <div className="ipad m-0 w-100">
                            <img src={lotr3}></img>
                        </div>
                        <div className="single-ipad-extra"></div>
                    </div>
                    <div className="col-md-4 ipad-groups-column">
                        <div className="ipad m-0 w-100">
                            <img src={landInterstellar}></img>
                        </div>
                        <div className="single-ipad-extra"></div>
                    </div>
                    <div className="col-md-4 ipad-groups-column ">
                        <div className="ipad m-0 w-100">
                            <img src={wonderwoman}></img>
                        </div>
                        <div className="single-ipad-extra"></div>
                    </div>
                </div>

                <div className="d-flex justify-content-center mb-5 second-group-margin">
                    <div className="col-md-4 ipad-groups-column">
                        <div className="ipad m-0 w-100">
                            <img src={dataAnalysis}></img>
                        </div>
                        <div className="single-ipad-extra"></div>
                    </div>
                    <div className="col-md-4 ipad-groups-column">
                        <div className="ipad m-0 w-100">
                            {/* <video src={tiktokVideo} loop="true" autoPlay="true" muted></video> */}
                            <img src={landEarth}></img>
                        </div>
                        <div className="subtext-color font-weight-bold small-title-font mt-2 text-right">Designed by DiscoveryLand</div>
                        <div className="single-ipad-extra"></div>
                    </div>
                    <div className="col-md-4 ipad-groups-column">
                        <div className="ipad m-0 w-100">
                            <img src={landRes}></img>
                        </div>
                        <div className="single-ipad-extra"></div>
                    </div>
                    <div className="col-md-4 ipad-groups-column">
                        <div className="ipad m-0 w-100">
                            <img src={landTiktok}></img>
                        </div>
                        <div className="single-ipad-extra"></div>
                    </div>
                </div>
            </div>
        )
    }
}