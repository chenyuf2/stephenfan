import logo from './logo.svg';
import './App.scss';
import React, {Component} from 'react';
import Home from './component/Home/Home';
class App extends Component {
  render() {
    return (
      <div className="App">
          <Home></Home>
      </div>
    );
  }
}

export default App;
